# LITEOS_M-SHT3X温湿度传感器驱动

Data : 2021-2-27
Author : yhuan416

---
## 移植说明   

1 将该文件夹放到工程目录下  
2 在对应程序的BUILD.gn文件中添加头文件路径
```
    include_dirs = [
        ...
        "//applications/BearPi/BearPi-HM_Nano/sample/liteos_m-sht3x/include",
        ...
    ]
```
2 添加依赖选项  
```
    deps = [
        ...
        "//applications/BearPi/BearPi-HM_Nano/sample/liteos_m-sht3x:SHT3x",
        ...
    ]
```
3 编译

---
## 版本说明
Version : v1.0
第一次发布

### 功能  
1 支持启动周期测量模式  
2 支持管理多个sht30设备  

---

### 使用说明  
1 声明一个设备管理结构体
```
SHT3x dev;
```

2 配置设备对应的I2C总线和器件地址
```
dev.host = WIFI_IOT_I2C_IDX_1;
dev.slave = SHT3X_ADDR_VSS;/* 在头文件内定义,默认的器件地址 */
```

3 启动周期测量模式
```
SHT3X_StartPeriodicMeasurment(&dev, REPEATAB_HIGH, FREQUENCY_1HZ);/* 参数在头文件内定义 */
```

4 获取传感器采集到的数据
```
SHT3X_ReadMeasurementBuffer(&dev);
printf("Humi : %.2f\r\n", dev.data.humidity);
printf("Temp : %.2f\r\n", dev.data.temperature);
```

---

## 后续开发计划
1 添加另外两种测量模式  
2 完善文档  :)  
...  

---
